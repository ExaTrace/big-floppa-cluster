#!/bin/bash

CHROOT=/opt/ohpc/admin/images/leap15.5

# Add NFS client mounts of /home and /opt/ohpc/pub to base image
echo "172.21.222.62:/home /home nfs nfsvers=4,nodev,nosuid 0 0" >> $CHROOT/etc/fstab
echo "172.21.222.62:/opt/ohpc/pub /opt/ohpc/pub nfs nfsvers=4,nodev 0 0" >> $CHROOT/etc/fstab

# Export /home and OpenHPC public packages from master server
echo "/home *(rw,no_subtree_check,fsid=10,no_root_squash)" >> /etc/exports
echo "/opt/ohpc/pub *(ro,no_subtree_check,fsid=11)" >> /etc/exports
#If planning to install the Intel® oneAPI compiler runtime (see §4.7), register the following additional path
#(/opt/intel) to share with computes:

# (Optional) Setup NFS mount for /opt/intel if planning to install oneAPI packages
mkdir /opt/intel
echo "/opt/intel *(ro,no_subtree_check,fsid=12)" >> /etc/exports
echo "172.21.222.62:/opt/intel /opt/intel nfs nfsvers=4,nodev 0 0" >> $CHROOT/etc/fstab

# Finalize NFS config and restart
exportfs -a
systemctl restart nfs-server
systemctl enable nfs-server
