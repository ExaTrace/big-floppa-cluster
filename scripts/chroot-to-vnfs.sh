#!/bin/bash

# Define the VNFS directory (adjust this path to match your actual VNFS path)
VNFS_DIR="/opt/ohpc/admin/images/leap15.5"

# Mount necessary filesystems
sudo mount --bind /proc $VNFS_DIR/proc
sudo mount --bind /sys $VNFS_DIR/sys
sudo mount --bind /dev $VNFS_DIR/dev

# Enter the VNFS environment
sudo chroot $VNFS_DIR /bin/bash

# At this point, you are inside the VNFS environment and can run commands to verify its setup
# For example, check the network configuration, installed packages, etc.

ifconfig -a

rpm -qa

ls -l /etc
ls -l /opt/ohpc

hostname
date
