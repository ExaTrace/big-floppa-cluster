#!/bin/bash

CHROOT=/opt/ohpc/admin/images/leap15.5

zypper -n --root $CHROOT --no-gpg-checks --gpg-auto-import-keys refresh

# copy credential files into $CHROOT to ensure consistent uid/gids for slurm/munge at
# install. Note that these will be synchronized with future updates via the provisioning system.
cp /etc/passwd /etc/group $CHROOT/etc

# Add OpenHPC base components
zypper -n --root $CHROOT install ohpc-base-compute

# Add SLURM client support meta-package and enable munge and slurm
zypper -n --root $CHROOT install ohpc-slurm-client
chroot $CHROOT systemctl enable munge
chroot $CHROOT systemctl enable slurmd

# Register Slurm server with computes (using "configless" option)
echo SLURMD_OPTIONS="--conf-server 172.21.222.62" > $CHROOT/etc/sysconfig/slurmd

# Provide udev rules to enable /dev/ipath access for InfiniPath devices
cp /opt/ohpc/pub/examples/udev/60-ipath.rules $CHROOT/etc/udev/rules.d/

# Add Network Time Protocol (NTP) support
zypper -n --root $CHROOT install chrony
chroot $CHROOT systemctl enable chronyd

#Identify master host as local NTP server
echo "server 172.21.222.62 iburst" >> $CHROOT/etc/chrony.conf

# Add kernel drivers
zypper -n --root $CHROOT install kernel-default

# Include modules user environment
zypper -n --root $CHROOT install lmod-ohpc

# Enable ssh access
chroot $CHROOT systemctl enable sshd.service

# Remove default hostname to allow WW to provision network names
mv $CHROOT/etc/hostname $CHROOT/etc/hostname.orig
