#!/bin/bash

# Define necessary variables
eth_provision="em1"      # Provisioning network interface
num_computes=22          # Number of compute nodes
compute_prefix="m"       # Prefix for compute node names

# Node names and IP/MAC addresses from the provided list
c_name=("m1.cluster" "m2.cluster" "m3.cluster" "m4.cluster" "m5.cluster" "m6.cluster" "m7.cluster" "m8.cluster" "m9.cluster" "m10.cluster" "m11.cluster" "m12.cluster" "m13.cluster" "m14.cluster" "m15.cluster" "m16.cluster" "m17.cluster" "m18.cluster" "m19.cluster" "m20.cluster" "m21.cluster" "m22.cluster")
c_ip=("172.21.222.63" "172.21.222.64" "172.21.222.65" "172.21.222.66" "172.21.222.67" "172.21.222.68" "172.21.222.69" "172.21.222.70" "172.21.222.71" "172.21.222.72" "172.21.222.73" "172.21.222.74" "172.21.222.75" "172.21.222.76" "172.21.222.77" "172.21.222.78" "172.21.222.79" "172.21.222.80" "172.21.222.81" "172.21.222.82" "172.21.222.83" "172.21.222.84")
c_mac=("14:18:77:61:1a:2c" "24:6e:96:56:87:d8" "24:6e:96:8d:cb:c8" "14:18:77:61:3d:c1" "24:6e:96:56:06:48" "24:6e:96:8d:d6:50" "24:6e:96:8d:d6:00" "14:18:77:61:0b:aa" "14:18:77:61:1d:df" "14:18:77:61:25:61" "14:18:77:61:11:91" "14:18:77:61:0d:d8" "24:6e:96:56:03:a8" "24:6e:96:56:36:c0" "24:6e:96:56:1c:58" "24:6e:96:56:05:90" "24:6e:96:56:06:f0" "24:6e:96:56:85:40" "24:6e:96:8d:c6:18" "24:6e:96:56:11:38" "24:6e:96:8d:db:c8" "24:6e:96:8d:db:50")

# Set provisioning interface as the default networking device
#echo "GATEWAYDEV=${eth_provision}" > /tmp/network.$$
#sudo wwsh -y file import /tmp/network.$$ --name network
#sudo wwsh -y file set network --path /etc/sysconfig/network --mode=0644 --uid=0
#rm /tmp/network.$$

# Add nodes to Warewulf data store
#for ((i=0; i<$num_computes; i++)); do
#  sudo wwsh -y node new ${c_name[i]} --ipaddr=${c_ip[i]} --hwaddr=${c_mac[i]} -D ${eth_provision}
#done

export kargs="${kargs} net.ifnames=1,biosdevname=1"
wwsh provision set --postnetdown=1 "m[1-22]"

# Define provisioning image for hosts
sudo wwsh -y provision set "m[1-22]" --vnfs=leap15.5 --bootstrap=`uname -r` --files=dynamic_hosts,passwd,group,shadow,munge.key,network

# Restart dhcp and update PXE
#sudo systemctl restart dhcpd
#sudo wwsh pxe update

echo "Nodes registered and configured for provisioning."

