#!/bin/bash

# Define the base directory
BASE_DIR="cluster-setup"

# Create directory structure
mkdir -p $BASE_DIR/provisioning/warewulf
mkdir -p $BASE_DIR/provisioning/slurm
mkdir -p $BASE_DIR/provisioning/ntp
mkdir -p $BASE_DIR/provisioning/nfs
mkdir -p $BASE_DIR/provisioning/infiniband
mkdir -p $BASE_DIR/provisioning/logging
mkdir -p $BASE_DIR/provisioning/pam
mkdir -p $BASE_DIR/users
mkdir -p $BASE_DIR/bootstrap
mkdir -p $BASE_DIR/vnfs
mkdir -p $BASE_DIR/customizations/beegfs
mkdir -p $BASE_DIR/customizations/lustre
mkdir -p $BASE_DIR/customizations/magpie
mkdir -p $BASE_DIR/customizations/geopm
mkdir -p $BASE_DIR/customizations/other
mkdir -p $BASE_DIR/management-tools/clustershell
mkdir -p $BASE_DIR/management-tools/genders
mkdir -p $BASE_DIR/management-tools/conman
mkdir -p $BASE_DIR/management-tools/nhc

# Copy configuration files
cp /etc/warewulf/provision.conf $BASE_DIR/provisioning/warewulf/
cp /etc/slurm/slurm.conf $BASE_DIR/provisioning/slurm/
cp /etc/chrony.conf $BASE_DIR/provisioning/ntp/
cp /etc/exports $BASE_DIR/provisioning/nfs/
cp /etc/sysconfig/network/ifcfg-en0 $BASE_DIR/provisioning/infiniband/
cp /etc/modprobe.d/10-unsupported-modules.conf $BASE_DIR/provisioning/
cp /etc/rsyslog.d/ohpc.conf $BASE_DIR/provisioning/logging/
cp /etc/rsyslog.conf $BASE_DIR/provisioning/logging/
cp /etc/pam.d/sshd $BASE_DIR/provisioning/pam/

# Copy user and group information
cp /etc/passwd $BASE_DIR/users/
cp /etc/group $BASE_DIR/users/
cp /etc/shadow $BASE_DIR/users/
cp /etc/munge/munge.key $BASE_DIR/users/

# Copy bootstrap and VNFS commands
echo "wwvnfs --chroot \$CHROOT" > $BASE_DIR/bootstrap/commands.txt
echo "wwvnfs --chroot \$CHROOT" > $BASE_DIR/vnfs/commands.txt

# Print completion message
echo "Directory structure and file copying complete."

